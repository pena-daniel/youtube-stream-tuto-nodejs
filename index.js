import express from "express"
import { createReadStream, stat } from "fs"
import { extname, resolve } from "path"
import { promisify } from "util"
const app = express()
const port = 3000


app.get('/', (req, res) => {
  res.sendFile(resolve('public', 'index.html'))
})
app.get('/api/stream', async (req, res) => {
  if(!req.query.video || !req.query.video.match(/^[a-z0-9-_ ]+\.(mkv|mov|mp4)$/i)){
    res.status(404)
    .send('nous ne trouvons pas votre demande')
    // console.error('error')
  }

  const video = resolve('videos', req.query.video);
  const range = req.headers.range
  const parts = range.replace('bytes=', '').split('-')
  const infos = await promisify(stat)(video)
  const start = parseInt(parts[0], 10)
  const end = parts[1] ? parseInt(parts[1],10) : infos.size - 1
  const headers = {
    "Content-Range": `bytes ${start}-${end}/${infos.size}`,
    "Accept-Ranges": "bytes",
    "Content-Length": end - start + 1,
    "Content-type": extname(video)
  } 
  res.writeHead(206, headers)
  const stream = createReadStream(video, {start, end})
  
  stream.pipe(res)
})
app.listen(port, () => {
  console.log(`app listening on port ${port}`)
})